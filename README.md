# Beamer theme for UBB IDDE slides

## Necessary tools

- a valid [TeXLive](https://www.tug.org/texlive/) distribution

## Installation from repo

```
./install.sh
```

## Downloading packaged version

```bash
wget --output-document=theme.zip https://gitlab.com/bbte-mmi/idde/theme/-/jobs/artifacts/master/download?job=deploy-artifact
unzip -ud `kpsewhich -var-value=TEXMFHOME` theme.zip
```

## Using the template

```tex
\documentclass{beamer}

% ...

\usetheme{idde}

\title{Presentation Title}
\subtitle{Subtitle}
\author{Name Surname}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

% ...

\end{document}
```
