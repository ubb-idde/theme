#!/bin/bash

# --------------------------------------------------------
# install idde theme to local TeXLive distribution
# --------------------------------------------------------

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# find local texmf tree
texmf_home=`kpsewhich -var-value=TEXMFHOME`

# copy everything into local TexMF tree
mkdir -p $texmf_home/tex
mkdir -p $texmf_home/fonts
cp -rfv $script_dir/tex/* $texmf_home/tex
cp -rfv $script_dir/fonts/* $texmf_home/fonts

# rehash so new lib will be found
texhash $texmf_home